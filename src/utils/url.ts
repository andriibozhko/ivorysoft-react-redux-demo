interface FilterObject {
    [key: string]: string;
}

export const generateFilterURL = (filterObject: FilterObject, search: string): string => {
    const queryParams = new URLSearchParams(search);
    Object.entries(filterObject).forEach(([key, value]) => {
        if (!value) return
        queryParams.set(key, value);
    });

    return `?${queryParams.toString()}`;
};

export const parseFilterURL = (url: string): FilterObject => {
    const searchParams = new URLSearchParams(url.split('?')[1]);
    const filterObject: FilterObject = {};

    searchParams.forEach((value, key) => {
        filterObject[key] = value;
    });

    return filterObject;
};
import { useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import { AppDispatch } from "../../store";
import { parseFilterURL } from '../../utils/url';

import { fetchExercisesData } from "../../store/slices/exercises";
import { selectExercises } from "../../store/selectors/exercises";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import routes from "../../constants/routes";

import {
    CircularProgress,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Box,
    Typography
} from '@mui/material'

interface Column {
    id: 'name' | 'difficulty' | 'type' | 'equipment' | 'instructions';
    label: string;
    minWidth?: number;
    align?: 'right' | 'center' | 'left';
    format?: (value: number) => string;
}

const columns: readonly Column[] = [
    { id: 'name', label: 'Name', minWidth: 150, align: 'center' },
    { id: 'difficulty', label: 'Difficulty', minWidth: 100, align: 'center' },
    {
        id: 'type',
        label: 'Type',
        minWidth: 100,
        align: 'center',
    },
    {
        id: 'equipment',
        label: 'Equipment',
        minWidth: 170,
        align: 'center',
    },
    {
        id: 'instructions',
        label: 'Instructions',
        minWidth: 300,
        align: 'center',
    },
];


const ExercisesTable = () => {
    const dispatch = useDispatch<AppDispatch>();
    const { search } = useLocation();

    const { data, isLoading, errors } = useSelector(selectExercises)

    useEffect(() => {
        dispatch(fetchExercisesData(parseFilterURL(search)))
    }, [dispatch, search]);

    useEffect(() => {
        if (errors) {
            toast.error(errors.message)
        }
    }, [errors]);

    if (isLoading) {
        return (
            <Box justifyContent="center" alignItems="center" display="flex" height="100vh">
                <CircularProgress size={100} />
            </Box>
        )
    }

    if (!data.length) {
        return (
            <Box justifyContent="center" flexDirection="column" alignItems="center" display="flex" height="100vh">
                <Typography color="white" fontSize="36px">
                    No result found.
                </Typography>
                <Link to={routes.ROOT}>
                    <Typography color="white" fontSize="25px">
                        Go Back.
                    </Typography>
                </Link>
                <ToastContainer

                />
            </Box>
        )
    }

    return (
        <Box sx={{ width: '80%', margin: 'auto' }}>
            <Box marginTop="50px">
                <Link to={routes.ROOT}>
                    <Typography color="white" fontSize="25px">
                        Go Back.
                    </Typography>
                </Link>
            </Box>
            <Paper sx={{ overflow: 'hidden', margin: '10px 0' }}>
                <TableContainer>
                    <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                                {columns.map((column) => (
                                    <TableCell
                                        key={column.id}
                                        align={column.align}
                                        style={{ minWidth: column.minWidth }}
                                    >
                                        {column.label}
                                    </TableCell>
                                ))}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {data
                                .map((row) => {
                                    return (
                                        <TableRow hover role="checkbox" tabIndex={-1} key={row.name}>
                                            {columns.map((column) => {
                                                const value = row[column.id];
                                                return (
                                                    <TableCell key={column.id} align={column.align}>
                                                        {value}
                                                    </TableCell>
                                                );
                                            })}
                                        </TableRow>
                                    );
                                })}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
        </Box>
    )
}

export default ExercisesTable;
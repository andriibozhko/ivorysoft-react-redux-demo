import { Box } from '@mui/material'

import ExerciseForm from '../../components/ExerciseForm';

const Root = () => {
    return (
        <Box sx={{
        }}>
            <ExerciseForm />
        </Box>
    )
}

export default Root;
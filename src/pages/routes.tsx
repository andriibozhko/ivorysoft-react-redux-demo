import Root from './Root';
import ExercisesTable from './ExercisesTable';
import ErrorPage from './ErrorPage';
import routesPath from '../constants/routes';

const routes = [
    {
        path: routesPath.ROOT,
        element: <Root />,
        errorElement: <ErrorPage />,
    },
    {
        path: routesPath.EXERCISES,
        element: <ExercisesTable />
    }
]

export default routes;
import { useForm, Controller } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { yupResolver } from '@hookform/resolvers/yup';
import { FormControl, InputLabel, Select, MenuItem, Button, TextField, Container, } from '@mui/material';
import { useLocation } from 'react-router-dom';

import { ExerciseFormData } from '../../types/exercises';
import { generateFilterURL } from '../../utils/url';

import { MUSCLE_GROUPS, EXERCISE_TYPES, DIFFICULTY_LEVELS } from '../../constants/exercises';

import schema from './schema';

const ExerciseForm = () => {
    const navigate = useNavigate();
    const { search } = useLocation();

    const { handleSubmit, control, watch } = useForm<ExerciseFormData>({
        resolver: yupResolver(schema),
    });

    const allFields = watch();

    const onSubmit = (data: ExerciseFormData) => {
        navigate(`/exercises${generateFilterURL(data, search)}`);
    };

    return (
        <Container maxWidth="sm" sx={{
            backgroundColor: 'white',
            padding: 5,
            marginTop: 10,
            borderRadius: 2
        }}>
            <form onSubmit={handleSubmit(onSubmit)}>
                <FormControl fullWidth margin="normal">
                    <Controller
                        name="name"
                        control={control}
                        defaultValue=''
                        render={({ field }) => (
                            <TextField
                                {...field}
                                label="Name"
                                name="name"
                                variant="outlined"
                            />
                        )}
                    />
                </FormControl>
                <FormControl fullWidth margin="normal">
                    <InputLabel>Type</InputLabel>
                    <Controller
                        name="type"
                        control={control}
                        defaultValue=''
                        render={({ field }) => (
                            <Select {...field} label="Type" >
                                <MenuItem value="">Select Type</MenuItem>
                                {EXERCISE_TYPES.map(({ value, label }) => (
                                    <MenuItem key={value} value={value}>{label}</MenuItem>
                                ))}
                            </Select>
                        )}
                    />
                </FormControl>
                <FormControl fullWidth margin="normal">
                    <InputLabel>Muscle</InputLabel>
                    <Controller
                        name="muscle"
                        control={control}
                        defaultValue=''
                        render={({ field }) => (
                            <Select {...field} label="Muscle" >
                                <MenuItem value="">Select Muscle</MenuItem>
                                {MUSCLE_GROUPS.map(({ value, label }) => (
                                    <MenuItem key={value} value={value}>{label}</MenuItem>
                                ))}
                            </Select>
                        )}
                    />
                </FormControl>
                <FormControl fullWidth margin="normal">
                    <InputLabel>Difficulty</InputLabel>
                    <Controller
                        name="difficulty"
                        control={control}
                        defaultValue=''
                        render={({ field }) => (
                            <Select {...field} label="Difficulty" >
                                <MenuItem value=''>Select Difficulty</MenuItem>
                                {DIFFICULTY_LEVELS.map(({ value, label }) => (
                                    <MenuItem key={value} value={value}>{label}</MenuItem>
                                ))}
                            </Select>
                        )}
                    />
                </FormControl>
                <Button type="submit" variant="contained" color="primary" disabled={!Object.keys(allFields).length}
                    sx={{
                        width: '100%',
                        height: 50,
                        marginTop: 5
                    }}>
                    Search
                </Button>
            </form>
        </Container>
    );
};

export default ExerciseForm;

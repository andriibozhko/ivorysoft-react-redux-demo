import * as yup from 'yup';

import {
    ExerciseType,
    MuscleGroup,
    DifficultyLevel
} from '../../types/exercises';

export default yup.object().shape({
    name: yup.string(),
    type: yup.string().oneOf(['','cardio', 'olympic_weightlifting', 'plyometrics', 'powerlifting', 'strength', 'stretching', 'strongman'] as ExerciseType[]),
    muscle: yup.string().oneOf(['','abdominals', 'abductors', 'adductors', 'biceps', 'calves', 'chest', 'forearms', 'glutes', 'hamstrings', 'lats', 'lower_back', 'middle_back', 'neck', 'quadriceps', 'traps', 'triceps'] as MuscleGroup[]),
    difficulty: yup.string().oneOf(['', 'beginner', 'intermediate', 'expert'] as DifficultyLevel[]),
});
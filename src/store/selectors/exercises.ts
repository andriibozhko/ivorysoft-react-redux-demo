import { createDraftSafeSelector } from '@reduxjs/toolkit'
import { RootState } from '..'

export const selectExercises = createDraftSafeSelector((state: RootState) => state, (state) => state.exercises)
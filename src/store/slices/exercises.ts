import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

import api from '../../api'
import { Exercise, ExerciseFormData } from '../../types/exercises';

type Errors = {
  message: string;
}

export interface ExercisesState {
  data: Exercise[];
  isLoading: boolean;
  errors: Errors | null,
}

const initialState: ExercisesState = {
  data: [],
  isLoading: true,
  errors: null
}

export const fetchExercisesData = createAsyncThunk(
  'exercises/fetchExercisesData',
  async (params: ExerciseFormData) => {
    const { data } = await api.get('/exercises', {
      params,
    });
    return data;
  }
);

export const exercisesSlice = createSlice({
  name: 'exercises',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchExercisesData.fulfilled, (state, action) => {
      state.data = action.payload;
      state.isLoading = false;
    })
    builder.addCase(fetchExercisesData.pending, (state) => {
      state.isLoading = true;
    })
    builder.addCase(fetchExercisesData.rejected, (state, action) => {
      state.isLoading = false;
      state.errors = {
        message: action.error.message || 'Something Wrong'
      }
    })
  }
})

export default exercisesSlice.reducer
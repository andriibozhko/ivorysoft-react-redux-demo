export type ExerciseType = "cardio" | "olympic_weightlifting" | "plyometrics" | "powerlifting" | "strength" | "stretching" | "strongman" | "";

export type MuscleGroup = "abdominals" | "abductors" | "adductors" | "biceps" | "calves" | "chest" | "forearms" | "glutes" | "hamstrings" | "lats" | "lower_back" | "middle_back" | "neck" | "quadriceps" | "traps" | "triceps" | "";

export type DifficultyLevel = "beginner" | "intermediate" | "expert" | "";

export type ExerciseFormData = {
  name?: string;
  type?: ExerciseType;
  muscle?: MuscleGroup;
  difficulty?: DifficultyLevel;
}

export interface Exercise {
  difficulty: string;
  equipment: string;
  instructions: string;
  muscle: string;
  name: string;
  type: string;
}
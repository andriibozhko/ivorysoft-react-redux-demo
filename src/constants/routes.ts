const routes = {
    ROOT: '/',
    EXERCISES: '/exercises'
}

export default routes;